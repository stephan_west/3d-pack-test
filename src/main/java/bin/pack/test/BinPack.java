package bin.pack.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.Collectors;

import com.github.skjolber.packing.Box;
import com.github.skjolber.packing.BoxItem;
import com.github.skjolber.packing.Container;
import com.github.skjolber.packing.LargestAreaFitFirstPackager;
import com.github.skjolber.packing.Packager;
import com.github.skjolber.packing.Space;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class BinPack {

	public void testPack() {
		List<Container> containers = List.of(
			new Container(1000, 1000, 100, 100), // x y z and weight
			new Container(100, 100, 100, 100),
			new Container(1000, 1000, 1000, 100),
			new Container(1000, 1000, 1000, 100),
			new Container(1000, 100, 100, 100)
		);
		Packager packager = new LargestAreaFitFirstPackager(containers, false, true, true);

		// set the boxes
		List<BoxItem> products = List.of(
				new BoxItem(new Box("Milk", 60, 30, 40, 1), 30),
				new BoxItem(new Box("Eggs", 60, 30, 50, 2), 75),
				new BoxItem(new Box("Broken Eggs", 60, 30, 40, 2), 44),
				new BoxItem(new Box("Spoiled Milk", 2, 3, 4, 2), 100)
	    );

		// match multiple containers
		// limit search using 5 seconds deadline
		long deadline = System.currentTimeMillis() + 5000;
		List<Container> fits = packager.packList(products, containers.size(), deadline);
		Gson gsonBuilder = new GsonBuilder().setPrettyPrinting().create();
		fits.forEach((n) -> {
			List<PlacementModel> placementModelList = n.getLevels().stream().map(Collection::stream)
					.flatMap(placementStream -> placementStream
							.map(placement -> new BoxToSpace(placement.getBox(), placement.getSpace())))
					.map(boxToSpace -> new PlacementModel(boxToSpace.getBox().getName(), boxToSpace.getSpace().getX(),
							boxToSpace.getSpace().getY(), boxToSpace.getSpace().getZ(), boxToSpace.getBox().getWidth(),
							boxToSpace.getBox().getDepth(), boxToSpace.getBox().getHeight()))
					.collect(Collectors.toList());

			PackModel packModel = new PackModel(placementModelList, n.getWeight(), n.getWidth(), n.getDepth(),
					n.getHeight(), n.getVolume(), n.getName());
			System.out.println(gsonBuilder.toJson(packModel));
		});
	}

	private class BoxToSpace {
		private final Box box;
		private final Space space;

		protected BoxToSpace(Box box, Space space) {
			this.box = box;
			this.space = space;
		}

		public Box getBox() {
			return box;
		}

		public Space getSpace() {
			return space;
		}
	}
}