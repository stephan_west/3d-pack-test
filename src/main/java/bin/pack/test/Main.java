package bin.pack.test;

import com.github.skjolber.packing.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {
		BinPack packer = new BinPack();
		packer.testPack();
	}
}	